# -*- coding: utf-8 -*-
"""
@author: CY Peng

DataVer  - Author - Note
2019/05/29    CY    Yolo Model Set
2019/06/05    CY    Pretraining/Testing/Validation/Prediction OK
2019/06/07    CY    Training/Testing/Validation OK
2019/06/17    CY    IOU/Precision/Recall Check
2019/06/18    CY    Training Loss Issues
2019/06/25    CY    Preprocessing- Cropping/Rotation/Flipping
2019/06/30    CY    First Release  (20190630-R 1.0.0)
2019/11/27    CY    Add CMD Mode and IDE Mode, Path Update, Check
2019/12/01    CY    Second Release (20191201-R 1.0.0)
"""
import os
import sys
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import create_default_yolo_model as PreModel
import create_yolo_model as yolo_model
import preprocessingLib as PrePro
from configparser import ConfigParser
import re

mini_database_dir = os.path.join(".","mini_database") #".\\mini_database\\" #os.path.join(".","mini_database") #".\\mini_database\\"
database_dir = os.path.join(".","database")
model_output_folder = os.path.join(".","yolo","pretraining")
offical_weights_filename = os.path.join(".","offical_elements","yolov3.weights")
coco_labels_filename = os.path.join(".","offical_elements","coco.names")
TFRECORD_TRAIN_FILE = os.path.join(".","mini_database","voc_dataset_train")
TFRECORD_TEST_FILE = os.path.join(".","mini_database","voc_dataset_train")
TFRECORD_VALIDATE_FILE = os.path.join(".","mini_database","voc_dataset_val")
TRANSTRAIN_EPOCH_NUM = 10
TRANSTRAIN_SHUFFLE_SIZE = 150
TRANSTRAIN_REPEAT_NUM = 30
TRANSTRAIN_BATCH_SIZE = 8
pred_dir = ".\\pred\\"

def image_preprocessing_stage(voc_data1_folder = os.path.join(".","voc_data1"),
                              voc_data2_folder = os.path.join(".", "voc_data2"),
                              database_dir = database_dir,
                              tfrecord_train_file = 'voc_dataset_train',
                              tfrecord_val_file = 'voc_dataset_val',
                              image_check_flag = True, set_save_img1 = -1, set_save_img2 = -1):
    PrePro.voc2TFRecord(voc_data1_folder, database_dir, tfrecord_train_file, image_check_flag = image_check_flag, save_img_num=set_save_img1)
    PrePro.voc2TFRecord(voc_data2_folder, database_dir, tfrecord_val_file, image_check_flag = image_check_flag, save_img_num=set_save_img2)

def mini_image_preprocessing_stage(voc_data1_folder = os.path.join(".","voc_data1"),
                                   voc_data2_folder = os.path.join(".", "voc_data2"),
                                   mini_database_dir = mini_database_dir,
                                   tfrecord_train_file='voc_dataset_train',
                                   tfrecord_val_file='voc_dataset_val',
                                   image_check_flag = True, set_save_img1 = -1, set_save_img2 = -1):
    PrePro.minivoc2TFRecord(voc_data1_folder, mini_database_dir, tfrecord_train_file, image_check_flag = image_check_flag, save_img_num=set_save_img1)
    PrePro.minivoc2TFRecord(voc_data2_folder, mini_database_dir, tfrecord_val_file, image_check_flag = image_check_flag, save_img_num=set_save_img2)

def pretraining_recover_stage(model_output_folder=model_output_folder,
                              offical_weights_filename=offical_weights_filename):
    PreModel.ConvertWeights2tf(model_output_folder=model_output_folder,
                               yolo_weights_filename = offical_weights_filename)

def pretraining_testing_stage(coco_dataset_folder = os.path.join(".","coco_dataset"),
                              model_output_folder=model_output_folder,
                              coco_labels_filename = coco_labels_filename):
    PreModel.ModelPretrainingTest(coco_dataset_folder,
                                  model_base_folder=model_output_folder,
                                  coco_labels_file=coco_labels_filename)

def pretraining_prediction_stage(coco_pred_folder = os.path.join(".","coco_pred"),
                                 model_output_folder=model_output_folder,
                                 coco_labels_filename=coco_labels_filename):
    PreModel.model2prediction(coco_pred_folder,
                              model_base_folder=model_output_folder,
                              coco_labels_file=coco_labels_filename)

def transfer_learning_stage(train_file = TFRECORD_TRAIN_FILE,
                            test_file = TFRECORD_TEST_FILE,
                            epoch_num = TRANSTRAIN_EPOCH_NUM,
                            shuffle_size = TRANSTRAIN_SHUFFLE_SIZE,
                            repeat_num = TRANSTRAIN_REPEAT_NUM,
                            batch_size = TRANSTRAIN_BATCH_SIZE,
                            meta_dir = ".\\yolo\\pretraining\\convert\\",
                            output_dir = ".\\yolo\\",
                            total_train_num = 32, total_test_num = 32):
    yolo_model.TFRecord2TraningYoloModel(train_file,
                                         test_file,
                                         epoch_num,
                                         shuffle_size,
                                         repeat_num,
                                         batch_size,
                                         meta_dir=meta_dir,
                                         output_dir=output_dir,
                                         total_training_sample_num= total_train_num,
                                         total_test_sample_num= total_test_num)

def transfer_learning_recover_stage(train_file = TFRECORD_TRAIN_FILE,
                                    test_file = TFRECORD_TEST_FILE,
                                    epoch_num = TRANSTRAIN_EPOCH_NUM,
                                    shuffle_size = TRANSTRAIN_SHUFFLE_SIZE,
                                    repeat_num = TRANSTRAIN_REPEAT_NUM,
                                    batch_size = TRANSTRAIN_BATCH_SIZE,
                                    meta_dir = ".\\yolo\\model_1\\translearning\\",
                                    output_dir = ".\\yolo\\",
                                    total_train_num = 80, total_test_num = 80):
    yolo_model.TFRecord2TraningYoloModel(train_file,
                                         test_file,
                                         epoch_num,
                                         shuffle_size,
                                         repeat_num,
                                         batch_size,
                                         meta_dir=meta_dir,
                                         output_dir=output_dir,
                                         total_training_sample_num=total_train_num,
                                         total_test_sample_num=total_test_num)

def transfer_learning_val_stage(val_file = TFRECORD_TRAIN_FILE,
                                shuffle_size = TRANSTRAIN_SHUFFLE_SIZE,
                                repeat_num = TRANSTRAIN_REPEAT_NUM,
                                batch_size = TRANSTRAIN_BATCH_SIZE,
                                model_base_folder = ".\\yolo\\model_11\\",
                                n_classes = 2):
    yolo_model.ModelValidation(val_file,
                                model_base_folder,
                                shuffle_size,
                                repeat_num,
                                batch_size,
                                n_classes,
                                total_validation_sample_num=16)

def transfer_learning_prediction_stage(pred_dir = ".\\pred\\",
                                       model_base_folder = os.path.join(".","yolo","model_1")):
    class_names, classes_num = PrePro.mini_voc_class()
    yolo_model.model2prediction(pred_dir, class_names, model_base_folder)

"""
def webcam_application():
    class_names, classes_num = PrePro.mini_voc_class()
    yolo_model.webcam_model2prediction(class_names,
                                       ".\\yolo\\model_3\\")
"""

def main():
    mode = check_env()
    if (1 == mode):
        cmd_mode()
    if (2 == mode):
        ide_mode()
    #webcam_application()

def cmd_mode():
    # from ConfigParser import ConfigParser # for python3
    data_file = sys.argv[1]
    config = ConfigParser()
    config.read(data_file)
    kewgs = {}
    for para in config[config['Execution']['scope']]:
        pattern = re.compile(r'^[-+]?[-0-9]\d*\.\d*|[-+]?\.?[0-9]\d*$')
        flag = pattern.match(config[config['Execution']['scope']][para])
        if flag:
            try:
                kewgs[para] = float(config[config['Execution']['scope']][para])
            except:
                pass
            try:
                kewgs[para] = int(config[config['Execution']['scope']][para])
            except:
                pass
        else:
            kewgs[para] = config[config['Execution']['scope']][para]
    """
    for item in config.sections():
        kewgs[item] = {}
        for para in config[item]:
            kewgs[item][para] = config[item][para]
    """
    print('Parameters Setting {}'.format(kewgs))
    if 'pretraining_recover_stage'== config['Execution']['scope']:
        pretraining_recover_stage(**kewgs)
    if 'pretraining_testing_stage'== config['Execution']['scope']:
        pretraining_testing_stage(**kewgs)
    if 'pretraining_prediction_stage'== config['Execution']['scope']:
        pretraining_prediction_stage(**kewgs)
    if 'image_preprocessing_stage'== config['Execution']['scope']:
        image_preprocessing_stage(**kewgs)
    if 'mini_image_preprocessing_stage'== config['Execution']['scope']:
        mini_image_preprocessing_stage(**kewgs)
    if 'transfer_learning_stage'== config['Execution']['scope']:
        transfer_learning_stage(**kewgs)
    if 'transfer_learning_recover_stage'== config['Execution']['scope']:
        transfer_learning_recover_stage(**kewgs)
    if 'transfer_learning_val_stage'== config['Execution']['scope']:
        transfer_learning_val_stage(**kewgs)
    if 'transfer_learning_prediction_stage'== config['Execution']['scope']:
        transfer_learning_prediction_stage(**kewgs)
    print('Completed!')

def ide_mode():
    pretraining_recover_stage()
    pretraining_testing_stage()
    pretraining_prediction_stage()
    image_preprocessing_stage()  # max objects train:42 test:41
    mini_image_preprocessing_stage()
    transfer_learning_stage()
    transfer_learning_recover_stage()
    transfer_learning_val_stage()
    transfer_learning_prediction_stage()

def check_env():
    print('Check environment variables: {}'.format(sys.argv))
    if (len(sys.argv) > 1):
        print("CMD Mode, Author: CY")
        return 1
    else:
        print("IDE Mode, Author: CY")
        return 2

if __name__ == '__main__':
    main()

# -*- coding: utf-8 -*-
"""
@author: CY Peng

DataVer  - Author - Note
2019/06/05    CY    Pretraining/Testing/Validation/Prediction OK
"""
import os
from PIL import Image, ImageDraw, ImageFont
import glob
import postprocessingLib as PostPro
import tensorflow as tf
import numpy as np
import Yolo_v3 as yolo
import preprocessingLib as PrePro

def assign_batch_norm_var(assign_ops, assign_num, pre_weights, this_batch_norm_var):
    for var in this_batch_norm_var:
        shape = var.shape.as_list()
        num_params = np.prod(shape)
        var_weights = pre_weights[assign_num:assign_num + num_params]
        var_weights = var_weights.reshape(shape)
        assign_num += num_params
        assign_ops.append(tf.assign(var, var_weights))
    #print("Already Assign {} Parameters".format(assign_num))
    return assign_num

def assign_conv_weights(assign_ops, assign_num, pre_weights, this_conv_var):
    shape = this_conv_var.shape.as_list()
    num_params = np.prod(shape)
    var_weights = pre_weights[assign_num:assign_num + num_params]
    var_weights = var_weights.reshape((shape[3], shape[2], shape[0], shape[1]))
    var_weights = np.transpose(var_weights, (2, 3, 1, 0))
    assign_num += num_params
    assign_ops.append(tf.assign(this_conv_var, var_weights))
    #print("Already Assign {} Parameters".format(assign_num))
    return assign_num

def assign_conv_bias(assign_ops, assign_num, pre_weights, this_conv_var):
    shape = this_conv_var.shape.as_list()
    num_params = np.prod(shape)
    var_weights = pre_weights[assign_num:assign_num + num_params]
    var_weights = var_weights.reshape(shape)
    assign_num += num_params
    assign_ops.append(tf.assign(this_conv_var, var_weights))
    #print("Already Assign {} Parameters".format(assign_num))
    return assign_num

def assign_weights_no_bias(assign_ops, assign_num, pre_weights, model_variables, model_idx):
    this_conv_var = model_variables[model_idx]
    gamma, beta, mean, variance = model_variables[model_idx + 1:model_idx + 5]

    this_batch_norm_var = [beta, gamma, mean, variance]

    assign_num = assign_batch_norm_var(assign_ops, assign_num, pre_weights, this_batch_norm_var)
    assign_num = assign_conv_weights(assign_ops, assign_num, pre_weights, this_conv_var)
    return assign_num

def load_weights(model_variables, file_name):
    with open(file_name, "rb") as f:
        # Skip first 5 values containing irrelevant info
        np.fromfile(f, dtype=np.int32, count=5)
        pre_weights = np.fromfile(f, dtype=np.float32)
        assign_num, assign_ops = 0, []

        # Load weights for Darknet part.
        # Each convolution layer has batch normalization.
        for i in range(52):
            this_idx = 5*i
            assign_num = assign_weights_no_bias(assign_ops, assign_num, pre_weights, model_variables, this_idx)
        print("All Darnet Part Parameters Assign Completed!")

        # Loading weights for Yolo part.
        # 7th, 15th and 23rd convolution layer has biases and no batch norm.
        ranges = [range(0, 6), range(6, 13), range(13, 20)]
        unnormalized = [6, 13, 20]
        for j in range(3):
            for i in ranges[j]:
                this_idx = 52 * 5 + 5 * i + j * 2
                assign_num = assign_weights_no_bias(assign_ops, assign_num, pre_weights, model_variables, this_idx)

            this_conv_var = model_variables[52 * 5 + unnormalized[j] * 5 + j * 2 + 1]
            assign_num = assign_conv_bias(assign_ops, assign_num, pre_weights, this_conv_var)

            this_conv_var = model_variables[52 * 5 + unnormalized[j] * 5 + j * 2]
            assign_num = assign_conv_weights(assign_ops, assign_num, pre_weights, this_conv_var)
        print("All Detection Part Parameters Assign Completed!")

    return assign_ops

def ConvertWeights2tf(model_output_folder = ".\\output", yolo_weights_filename = '.\\yolov3.weights'):
    # Output Folder
    output_base_folder = model_output_folder + "\\convert\\"
    ckpt_filename = output_base_folder + "model.ckpt"
    log_dir = output_base_folder + "convert_logs\\"

    # Yolo Model
    yolo_model = yolo.Yolo_v3()

    # Definition: Global Model Common Variables
    model_vars = tf.global_variables(scope=yolo_model.model_name)
    assign_ops = load_weights(model_vars, yolo_weights_filename)

    convert_saver = tf.train.Saver(tf.global_variables(scope=yolo_model.model_name))

    with tf.Session() as convert_sess:
        # Run the tensor variables operation
        convert_writer = tf.summary.FileWriter(log_dir, convert_sess.graph)
        tf.train.write_graph(convert_sess.graph.as_graph_def(), model_output_folder, 'model.pbtxt', as_text=True)
        tf.train.write_graph(convert_sess.graph.as_graph_def(), model_output_folder, 'model.pb', as_text=False)
        convert_sess.run(assign_ops)
        convert_saver.save(convert_sess, ckpt_filename)

    # Free Memory
    convert_sess.close()
    convert_writer.close()
    print('Model has been saved successfully.')

def load_class_names(file_name):
    with open(file_name, 'r') as f:
        class_names = f.read().splitlines()
    return class_names

def load_images(img_names, model_size =(416, 416)):
    imgs = []

    for img_name in img_names:
        print(img_name)
        img = Image.open(img_name)
        img = img.resize(size=model_size)
        img = np.array(img, dtype=np.float32)
        img = np.expand_dims(img[:, :, :3], axis=0)
        imgs.append(img)

    imgs = np.concatenate(imgs)
    return imgs

def ModelPretrainingTest(coco_images_folder, iou_threshold = .5, confidence_threshold = .5,
                         model_base_folder= os.path.join(".","output"), coco_labels_file = os.path.join(".","coco.names")):
    # Validation - Dataset Preparation
    model_folder = os.path.join(model_base_folder, "convert")
    test_ckpt_file = os.path.join(model_folder, "model.ckpt")

    class_names = load_class_names(coco_labels_file)
    yolo_model = yolo.Yolo_v3(batch_size=1)
    prediction_op = PostPro.predict_boxes(yolo_model.ypred, 1)
    test_saver = tf.train.Saver(tf.global_variables(scope=yolo_model.model_name))
    check_flag = bool(1)

    filename_list = glob.glob(os.path.join(coco_images_folder,"*.jpg"))

    with tf.Session() as test_sess:
        test_saver.restore(test_sess, test_ckpt_file)
        for filename in filename_list:
            image_raw_array, image_array, raw_shape, shape_ratio, offset = PrePro.Images2array(filename)

            detection_result = test_sess.run(prediction_op, feed_dict={yolo_model.x: image_array})
            #print(len(detection_result))
            PostPro.pil_image_draw_boxes(filename, detection_result[0], class_names, shape_ratio, offset, check_flag)
            if check_flag:
                check_flag = bool(0)

        builder = tf.saved_model.builder.SavedModelBuilder(os.path.join(model_base_folder,"validation"))
        builder.add_meta_graph_and_variables(test_sess,
                                             [tf.saved_model.tag_constants.TRAINING],
                                             signature_def_map=None,
                                             assets_collection=None)
        builder.save()
        test_sess.close()
        print("Testing End...")

def model2prediction(coco_images_folder, model_base_folder= os.path.join(".","output"), coco_labels_file = os.path.join(".","coco.names")):
    # Prediction - Model Dataset Folder
    model_folder = os.path.join(model_base_folder, "validation")
    filename_list = glob.glob(os.path.join(coco_images_folder, "*.jpg"))
    class_names = load_class_names(coco_labels_file)
    check_flag = bool(1)

    with tf.Session() as pred_sess:
        tf.saved_model.loader.load(pred_sess, [tf.saved_model.tag_constants.TRAINING], model_folder)
        graph = tf.get_default_graph()
        x = graph.get_tensor_by_name("input/x:0")
        prediction_op = graph.get_tensor_by_name("yolo_v3/concat:0")

        for filename in filename_list:
            image_raw_array, image_array, raw_shape, shape_ratio, offset = PrePro.Images2array(filename = filename)
            #print(image_array.shape)
            prediction_output = pred_sess.run(prediction_op, feed_dict={x: image_array})
            detection_result = pred_sess.run(PostPro.predict_boxes(prediction_output, 1), feed_dict={x: image_array})
            PostPro.pil_image_draw_boxes(filename, detection_result[0], class_names, shape_ratio, offset, check_flag)
            if check_flag:
                check_flag = bool(0)

    print("Prediction End...")

#if __name__ == '__main__':
    #ConvertWeights2tf(model_output_folder = ".\\output",
    #                  yolo_weights_filename = '.\\yolov3.weights')
    #ModelPrediction()
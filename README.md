# Yolo-v3 (CY ver) Model in TensorFlow for Objects Recognition
<!-- blank line -->
<dl>
　<dd>C.Y. Peng</dd>
<!-- blank line -->
　<dd>Keywords: Deep Learning, YOLOv3 Model, TensorFlow Framework</dd> 
</dl> 


## Lib Information
- Release Ver: 20191201-R 1.0.0
- Lib Ver: 20191201
- Author: C.Y. Peng
- Required Lib: requirements_venv36dl.txt
- OS Required: Windows 64 bit
- [VOC PASCAL Datasets Required](http://host.robots.ox.ac.uk/pascal/VOC/)
- [Official Pretraining Weights Required](https://drive.google.com/drive/folders/1NCfZTycqMVNHSQF2jxcAlWww5KMlnRCf?usp=sharing)

## Part I. Overview
In this project, implementation of Yolo-v3 images/camera objects detector in Tensorflow (tf.layer module).
We cover several segments as follows:
- [x] Fundemental Principle
   - [x] Introduction to the Objects Recognition
   - [x] You Only Look at Once Version 3, Yolo-v3
   - [x] Open Images Datasets
- [x] Quick Start
   - [x] Project Lib Architecture
   - [x] Model Established Pipeline
   - [x] Operation Mode
- [x] Other Records
- [x] Reference

Yolo-v3 paper is a famous architecture in objects recognition, along side that paper. This repo enables you to have a quick understanding of Yolo-v3 algorithmn for images recognition.

## Part II. Fundemental Principle
### Introduction to the Objects Recognition
There are two main technology to detect the objects recognition [1-6], one is the Region Convolution Neural Network, RCNN. 
The another is the You Only Look at Once, Yolo. Yolo method is a pure CNN architecture, unlike two stages learning system - RCNN system, it is the end to end deep learning system. On the other hand, it is the real time 
multiple objects recognition technology.

### You Only Look at Once Version 3, Yolo-v3
#### Main Blocks of the Yolo-v3 
According to the reference [1-6], there are main fundemental four blocks in the Yolo-v3 architecture as figures:
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/ConvolutionBlock.png" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 1. Convolution Block Architecture [1-6]
</div>
<!-- blank line -->

1. Convolution Block (as Figure 1.): The data pipeline passing the convolution layers, the batch normalization and the LeakyReLU activation layer to extract the features.
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/ResidualBlock.png" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 2. Residual Block Architecture [1-6]
</div>
<!-- blank line -->

2. Residual Block (as Figure 2.): Using the Convolution block and the ResNet unit to established the Residual Block. This block is the main block in the Darknet block.
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/darknet53.png" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 3. Darknet 53 Architecture [1-6]
</div>
<!-- blank line -->

3. Darknet53 Block (as Figure 3.): Using the Convolution block and the ResNet block form the darknet53 block. There are three outputs in the Darknet, route1, route2 and output route.
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/darknet53_detail.png" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 4. Darknet 53 Detail [1-6]
</div>
<!-- blank line -->

More detail about the darknet53 [1] as Figure 4, there are 53 layers in the darknet block to extract the features and to established deeper learning net.

4. Upsampling Block: To expand the features map

#### Yolo-v3 Architecture 
According to the reference [1-6], there are three stages in Yolo-v3 as following as Figure 5.:
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/Yolo_v3_Architecture.png" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 5. Yolo-v3 Architecture [1-6]
</div>
<!-- blank line -->

1. First Stage: Data pipeline passing the Darknet 53 block to extract fine-grained features and to expand the higher-dimensional features.
2. Second Stage: To expand the features map through the convolution block and the upsampling block from the darknet 53 different outputs, this networks detect the multi-scale objects features, also called the feature pyramid network, FPN.
3. Third Stage: To predicted the object bounding box center, length, the recognition confidence score and a probability of object class by pure convolution layer.

Some Different between Original Yolo-v3 and This Project (Yolo-v3 CY ver.):
1. Original Yolo implementation is darknet in Caffe framework, the operation size is (out_channels, in_channels, kernel_height, kernel_width)
   For this project, in tensorflow framework, the operation size is (kernel_height, kernel_width, in_channels, out_channels)
2. Original Yolo use the left and top padding, in this project using the "SAME" method padding    

#### Workflow for Yolo-v3
Split an image to $`S*S`$ cells, there are three size for the grid cells: $`S=13, 26\ and\ 52`$, as figure 5. In each cells, if an object’s center falls into a cell, that cell is “responsible” for detecting the existence of that object. 
Each cell predicts the location of the bounding box center, confidence score, and a probability of object class conditioned on the existence of an object in the bounding box. 

#### Mean Average Precision, MAP
In this project, we use the precision, recall and iou to measure the prediction performance, as following figure:
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/Yolo_v3_Measure.png" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 6. Yolo-v3 Measurement [1-6]
</div>
<!-- blank line -->

#### Loss Function of Yolo-v3
We use the loss function as the backpropagation optimization, let:  
$`x, y: Bounding\ Box\ Coordinate\ Center\\`$  
$`w, h: Bounding\ Box\ Width\ and\ Heigth\\`$  
$`C: Object\ Confidence\ Score, C=P(Obj)*IOU\\`$  
$`p(c): Probability\ of\ Object\ Class\\`$  
$`1_{i}^{obj}:Object\ Appears\ in\ Cell\ i\\`$  
$`1_{ij}^{obj}:jth\ Bounding\ Box\ Predictor\ in\ Cell\ i\ is\ Responsible\\`$  
$`L_{coord_{xy}}: Object\ Center\ Loss\\`$  
$`L_{coord_{wh}}: Bounding\ Box's\ Width\ and\ Height\ Loss\\`$  
$`L_{coord}: Coordinate\ Loss\\`$  
$`L_{confid_{obj}}: Object\ Confidence\ Loss\\`$  
$`L_{confid_{noobj}}: No\ Object\ Confidence\ Loss\\`$  
$`L_{confid}: Confidence\ Loss\\`$   
$`L_{class}: Class\ Loss\\`$  
<!-- ![image](./ReadMe/defdenote.png)-->

There are three parts of the loss function [1-6]:

- Coordinate Loss: sum square error of the bounding box center's coordinate, width and length, only the object in the cell to calculate the loss value, as following formula:  
$`L_{coord_{xy}} = \lambda_{coord} \sum_{i=0}^{S^{2}}\sum_{j=0}^{B}1_{ij}^{obj}[(x_i-\hat{x_i})^2+(y_i-\hat{y_i})^2]`$  
$`L_{coord_{wh}} = \lambda_{coord} \sum_{i=0}^{S^{2}}\sum_{j=0}^{B}1_{ij}^{obj}[(\sqrt{w_i}-\sqrt{\hat{w_i}})^2+(\sqrt{h_i}-\sqrt{\hat{h_i}})^2]`$  
$`L_{coord}=L_{coord_{xy}}+L_{coord_{wh}}`$  
<!--![image](./ReadMe/Lcoord_xy.png)

![image](./ReadMe/Lcoord_wh.png)

![image](./ReadMe/Lcoord.png)-->

- Confidence Loss: sum square error of the confidence related to the iou as object in the cell, sum square error of the confidence as no object in the cell, as following formula:  
$`L_{confid_{obj}} = \sum_{i=0}^{S^{2}}\sum_{j=0}^{B}1_{ij}^{obj}(C_i-\hat{C_i})^2`$  
$`L_{confid_{noobj}} = \lambda_{noobj} \sum_{i=0}^{S^{2}}\sum_{j=0}^{B}1_{ij}^{noobj}(C_i-\hat{C_i})^2`$  
$`L_{confid}=L_{confid_{obj}}+L_{confid_{noobj}}`$  
<!--
![image](./ReadMe/Lconfid_obj.png)

![image](./ReadMe/Lconfid_noobj.png)

![image](./ReadMe/Lconfid.png)
-->
- Classification Loss: sum square error of the probability of object class, only the object in the cell to calculate the loss value, as following formula:  
$`L_{class} =  \sum_{i=0}^{S^{2}}1_{i}^{obj}\sum_{c\epsilon classes}(p_i(c)-\hat{p_i}(c))^2`$  
<!--
![image](./ReadMe/Lclass.png)
-->
### Open Images Datasets
It is necessary to training large dataset to establish the objects recognition system, but it is bit difficult to collecting the large images datasets by myself;
therefore, there are many open images datasets [5] for public providing, we can use these resourse to trainining/testing/validating the model.
For this project, we use the pretraining model for the MS COCO datasets, and we use the PASCAL VOC datasets to transfer learning the new model. 
	 
## Part III. Quick Start
In this project, we show the several functions of the Yolo-v3 (CY ver) from recovering model to the training/testing/validation as following figure:
<!-- blank line -->
<table align="center"><tr><td align="center" width="9999">
<img src="./ReadMe/quickStart.png" align="center" alt="Project icon">
</td></tr></table>    
<div align="center">

Figure 7. Yolo-v3 CY ver Lib Function Flow
</div>
<!-- blank line -->

There are two operation modes in this project: integrated development environment mode (IDE mode) and windows command prompt mode (CMD mode).

### Project Lib Architecture
There are two files requirements:
VOC PASCAL Datasets: Put these datasets to the voc_data1 folder (training/testing) and voc_data2 folder (validation)
Official Pretraining Weights: Put this model to the offical_elements folder

In this project, we need the three librarys:
- CameraObjectsDetection: Objects Recognition Lib
  - od_main: main file (Python File)
  - create_default_yolo_model: coverting official darknet yolo architecture weights to the tensorflow, and testing/ validating the model (Python File)
  - create_yolo_model: using the PASCAL VOC datasets to transfer learning from the official model (Python File)
  - preprocessingLib: some functions are about the images preprocessing such as cropping, rotation, the flipping and so on. (including PASCAL VOC datasets processing) (Python File)
  - postprocessingLib: decode the prediction output processing (Python File)

- CommonLib: Common Utility Tool (Note: Using this library, it is important to setup for some cython files!)
  - TFrecord: TensorFlow TFrecord files reading and writing preprocessing (Python File)
 
- DeepLearningLib: Various CNN Architecture Model
  - Yolo_v3: Yolo-v3 model (Python File)
  - NetworkLib: Some neural networks elements (Python File)

### Model Established Pipeline
In this project, there are eights steps for objects recognition model established.
- image_preprocessing_stage: save all PASCAL VOC images to the TFrecord and images preprocessing.
- mini_image_preprocessing_stage: save mini part of the PASCAL VOC images to the TFrecord and images preprocessing.

- pretraining_recover_stage: recovering the variables/ graph from the official model.
- pretraining_testing_stage: testing the model and save it to the freezon model.
- pretraining_prediction_stage: using the model to predict the objects recognition.
- transfer_learning_stage: setting epoch number, repeat number, batch size and shuffle size to training/ testing the model
- transfer_learning_recover_stage: recovering to the traning stage from the last training check point. 
- transfer_learning_val_stage: validating the model and save it to the freezon model
- transfer_learning_prediction_stage: using the model to predict the objects recognition

### Operation Mode
#### IDE mode:
Excute the od_main.py in different IDE.
#### CMD mode:
- Set cmd_config.txt file for objects recognizer established.
- Excute command line in windows cmd, examples as the enable_hw.bat file.

## Part V. Other Records
### Some Notes
TensorBoard Command for Terminal:  
- tensorboard --logdir=path --host=127.0.0.1

Freezon Model Command for Terminal:  
- python freeze_graph.py --input_graph=*.pbtxt --input_checkpoint=*.ckpt --output_graph=*.pb --output_node_names=node_name  

If you have any questions or suggestions, please let me know, thank you. Enjoy it!

### History Notes
     DataVer    Author      Note
     20190529   C.Y. Peng   Yolo Model Set
     20190605   C.Y. Peng   Pretraining/Testing/Validation/Prediction OK
     20190607   C.Y. Peng   Training/Testing/Validation OK
     20190617   C.Y. Peng   IOU/Precision/Recall Check
	 20190618   C.Y. Peng   Training Loss Issues
	 20190630   C.Y. Peng   First Release
	 20191127   C.Y. Peng   Add CMD Mode and IDE Mode, Path Update, Check
     20191201   C.Y. Peng   Second Release (20191201-R 1.0.0)
	 
## Part VI. Reference
### Some Official/Papers/Books Reference
- [1] Joseph Redmon and Ali Farhadi, "YOLOv3: An Incremental Improvement, " Computer Vision and Pattern Recognition, April, 2018.
- [2] Joseph Redmon and Ali Farhadi, "YOLO9000: Better, Faster, Stronger, " Computer Vision and Pattern Recognition, December, 2016.
- [3] Joseph Redmon, Santosh Divvala, Ross Girshick and Ali Farhadi"You Only Look Once: Unified, Real-Time Object Detection, " Computer Vision and Pattern Recognition, June, 2015.
- [4] [What’s new in YOLO v3](https://towardsdatascience.com/yolo-v3-object-detection-53fb7d3bfe6b)
- [5] [Yolo-v3 Official Website](https://pjreddie.com/darknet/)
- [6] [Object Detection Model](https://lilianweng.github.io/lil-log/2018/12/27/object-detection-part-4.html)
- [7] [Comprehensive Collection Deep Learning Dataset](https://www.analyticsvidhya.com/blog/2018/03/comprehensive-collection-deep-learning-datasets/)

### Some Technique Blog Post
- [Darknet Official Github](https://github.com/AlexeyAB/darknet)
- [Bounding box object detectors: understanding YOLO, You Look Only Once](http://christopher5106.github.io/object/detectors/2017/08/10/bounding-box-object-detectors-understanding-yolo.html)
- [You Only Look once:Unified,Real-Time Object Dectection](https://www.cnblogs.com/zf-blog/p/8473844.html)
- [Yolo Precision/Recall/IOU](https://blog.csdn.net/hysteric314/article/details/54093734)
- [Yolo Notes](https://deep-learning-study-note.readthedocs.io/en/latest/Part%202%20(Modern%20Practical%20Deep%20Networks)/12%20Applications/Computer%20Vision%20External/YOLO.html#training)
- [Yolo Treaky](https://github.com/ultralytics/yolov3/blob/f0b4f9f4fb7af71076cc91c48cdf2c9043395ad8/utils/utils.py#L260-L269)
- [Yolo Architecture Notes](https://blog.csdn.net/qq_27825451/article/details/88971395)


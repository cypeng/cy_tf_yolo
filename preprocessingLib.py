# -*- coding: utf-8 -*-
"""
@author: CY Peng

DataVer  - Author - Note
2019/06/03    CY    Preprocessing OK
2019/06/16   CY     Some Bug Fixed
2019/06/25    CY  Preprocessing- Cropping/Rotation/Flipping
2019/11/27    CY  add the Images Check Flag
"""
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import xml.etree.ElementTree as ET
import struct
import numpy as np
import CommonUtility as CU
import cv2
import glob
#from PIL import Image
import TFRecord as tfr
import tensorflow as tf
import random

# Pascal VOC data Setting
def voc_all_class():
    classes_name = ["aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow",
                    "diningtable",
                    "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train", "tvmonitor"]
    classes_num = {'aeroplane': 0, 'bicycle': 1, 'bird': 2, 'boat': 3, 'bottle': 4, 'bus': 5,
                   'car': 6, 'cat': 7, 'chair': 8, 'cow': 9, 'diningtable': 10, 'dog': 11,
                   'horse': 12, 'motorbike': 13, 'person': 14, 'pottedplant': 15, 'sheep': 16,
                   'sofa': 17, 'train': 18, 'tvmonitor': 19}
    return classes_name, classes_num

def mini_voc_class():
    classes_name = ["person", 'car']
    classes_num = {'person': 0, 'car':1}
    return classes_name, classes_num

def __voc_image_2_tfrecord_format__(filename, idx, images, label):
    #print(idx, len(images))
    image_raw = images[idx].tostring()
    label_raw = label[idx].astype(np.float32).tolist()
    label_row = label[idx].shape[0]
    label_col = label[idx].shape[1]
    label_raw = np.array(label_raw)
    label_raw = label_raw.flatten()
    #print(label_raw)
    rows = images[idx].shape[1]
    cols = images[idx].shape[2]
    channel = images[idx].shape[3]
    img_tfr_format = tf.train.Example(
        features=tf.train.Features(
            feature={
                'filename': tfr.bytes_feature(filename),
                'height': tfr.int64_feature(rows),
                'width': tfr.int64_feature(cols),
                'channel': tfr.int64_feature(channel),
                'label_row': tfr.int64_feature(label_row),
                'label_col': tfr.int64_feature(label_col),
                'image_raw': tfr.bytes_feature(image_raw),
                'label_raw':tfr.floats_feature(label_raw)
            })
    )
    return img_tfr_format

def __tfrecord_2_image_format__(dataset):
    # Extract features using the keys set during creation
    features = {
        'filename': tf.FixedLenFeature([], tf.string),
        'height': tf.FixedLenFeature([], tf.int64),
        'width': tf.FixedLenFeature([], tf.int64),
        'channel': tf.FixedLenFeature([], tf.int64),
        'label_raw': tf.FixedLenFeature([270], tf.float32),
        'label_row': tf.FixedLenFeature([], tf.int64),
        'label_col': tf.FixedLenFeature([], tf.int64),
        'image_raw': tf.FixedLenFeature([], tf.string),
    }
    image_info = tf.parse_single_example(dataset, features)

    image = tf.decode_raw(image_info['image_raw'], tf.uint8)
    image = tf.reshape(image, (416, 416, 3))
    img_shape = tf.stack([image_info['height'], image_info['width'], image_info['channel']])
    label_shape = tf.stack([image_info['label_row'], image_info['label_col']])
    rawlabel = image_info['label_raw']#tf.decode_raw(image_info['label_raw'], tf.float32)
    rawlabel = tf.reshape(rawlabel, (45, 6))
    filename = tf.decode_raw(image_info['filename'], tf.uint8)  # .decoder('utf-8')

    return image, rawlabel, img_shape, label_shape

def voc_parse_xml(data_path, xml_file, classes_num):
    tree = ET.parse(xml_file)
    root = tree.getroot()
    image_path = ''
    labels = []

    for item in root:
        if item.tag == 'filename':
            image_path = os.path.join(data_path, 'VOC2007\\JPEGImages', item.text)
        elif item.tag == 'object':
            try:
                obj_name = item[0].text
                obj_num = classes_num[obj_name]
                xmin = int(item[4][0].text)
                ymin = int(item[4][1].text)
                xmax = int(item[4][2].text)
                ymax = int(item[4][3].text)
                labels.append([xmin, ymin, xmax, ymax, obj_num])
            except:
                pass

    if len(labels) > 0:
        return image_path, labels
    else:
        return None, None


def convert_to_string(image_path, labels):
    out_string = ''
    out_string += image_path
    for label in labels:
        for i in label:
            out_string += ' ' + str(i)
    out_string += '\n'
    return out_string

def set_voc_info_file(classes_num, data_dir = os.path.join(".","voc_data")):
    # Folder Path Setting
    output_filename = os.path.join(data_dir, 'pascal_voc.txt')
    data_path = os.path.join(data_dir, 'VOCdevkit')
    out_file = open(output_filename, 'w')

    xml_dir = data_path + '\\VOC2007\\Annotations\\' #os.path.join(data_path, "VOC2007", "Annotations") #data_path + '\\VOC2007\\Annotations\\'
    xml_list = os.listdir(xml_dir)
    xml_list = [xml_dir + temp for temp in xml_list]

    for xml in xml_list:
        try:
            image_path, labels = voc_parse_xml(data_path, xml, classes_num)
            if image_path is not None and labels is not None:
                record = convert_to_string(image_path, labels)
                out_file.write(record)
        except Exception:
            pass

    out_file.close()

    return output_filename

def croppingImages2array(filename, desired_size= 416, crop=0.9, angle=10, color = 0.05):
    # Shape Ratio
    image_raw = cv2.imread(filename, 3).astype(np.float)
    #image_raw_array = np.array(image_raw, dtype='uint8')
    yr, xr = image_raw.shape[:2]
    raw_shape = (xr, yr)

    # rotate
    rot_angle = random.uniform(-angle, +angle)
    Crot = cv2.getRotationMatrix2D((raw_shape[0] / 2, raw_shape[1] / 2), rot_angle, 1)
    image_raw = cv2.warpAffine(image_raw, Crot, raw_shape)

    # crop
    cropping_size = int(min(raw_shape[0], raw_shape[1]) * random.uniform(crop, 1))
    image_x_min = int(random.uniform(0, raw_shape[0] - cropping_size))
    image_y_min = int(random.uniform(0, raw_shape[1] - cropping_size))
    image_x_max = image_x_min + cropping_size
    image_y_max = image_y_min + cropping_size
    image_limit_shape = (image_x_min, image_y_min, image_x_max, image_y_max)
    image_raw = image_raw[image_y_min:image_y_max, image_x_min:image_x_max, :]

    # flip
    flip_para = random.random() < 0.5
    if flip_para:
        image_raw = cv2.flip(image_raw, 1)    

    # color
    r = random.uniform(1 - color, 1 + color)
    b = random.uniform(1 - color, 1 + color)
    g = random.uniform(1 - color, 1 + color)

    col = np.array([b, g, r])
    image_raw = image_raw * col
    image_raw[image_raw < 0] = 0
    image_raw[image_raw > 255] = 255

    # resize to inputsize
    new_shape, shape_ratio = equalRatio2DesiredSize((image_x_max-image_x_min, image_y_max-image_y_min),
                                                    desired_size=desired_size)
    # BGR Image Array
    b, g, r = cv2.split(image_raw)
    rgb_image_raw = cv2.merge([r, g, b])

    # BGR Reshape Image Array
    rgb_image_raw = cv2.resize(rgb_image_raw, new_shape)
    offset = getImageOffset(new_shape, desired_size=desired_size)

    color = [0, 0, 0]
    resize_image_raw = cv2.copyMakeBorder(rgb_image_raw, offset[0], offset[1], offset[2], offset[3],
                                          cv2.BORDER_CONSTANT, value=color)

    image_array = np.reshape(np.array(resize_image_raw, dtype='uint8'), (1, 416, 416, 3))

    return image_array, raw_shape, shape_ratio, offset, image_limit_shape, rot_angle, cropping_size, flip_para

def croppingInfo2rawlabel(label_info, raw_shape, max_group_num, shape_ratio, shape_offset, image_limit_shape,
                          rot_angle, cropping_size, flip_para):
    # classes = 20
    group_num = len(label_info) // 5
    label = np.zeros((max_group_num, 6))
    # print(label.shape)
    for idx in range(0, group_num):
        xmin, ymin, xmax, ymax, obj_num = label_info[5 * idx:5 * (idx + 1)]
        xmin, ymin, xmax, ymax = int(xmin), int(ymin), int(xmax), int(ymax)
        x0, y0 = (xmin + xmax) * 0.5/raw_shape[0], (ymin + ymax) * 0.5/raw_shape[1]
        w0, h0 = (xmax - xmin)/raw_shape[0], (ymax - ymin)/raw_shape[1]
        cls = int(obj_num)

        # Rotation
        rot_rad = np.deg2rad(rot_angle)
        Crot = np.array([[np.cos(rot_rad), np.sin(rot_rad)], [-np.sin(rot_rad), np.cos(rot_rad)]])
        x0, y0  = 0.5 + np.matmul(Crot, np.array([x0-0.5, y0-0.5]))

        # Cropping
        if x0 < image_limit_shape[0]/raw_shape[0] or \
           x0 > image_limit_shape[2]/raw_shape[0] or \
           y0 < image_limit_shape[1]/raw_shape[1] or \
           y0 > image_limit_shape[3]/raw_shape[1]:
            continue
        x0 = (x0*raw_shape[0] - image_limit_shape[0])/cropping_size
        y0 = (y0*raw_shape[1] - image_limit_shape[1])/cropping_size
        w0 = w0 *raw_shape[0] # / cropping_size
        h0 = h0 *raw_shape[1] #/ cropping_size

        # flipping
        if flip_para:
            x0 = 1 - x0        

        x0, y0 = (x0*cropping_size + shape_offset[2]) * shape_ratio[0], \
                 (y0*cropping_size + shape_offset[0]) * shape_ratio[1]
        w0, h0 = w0*shape_ratio[0], h0*shape_ratio[1]

        label[idx] = [1, x0, y0, w0, h0, cls]
    return label

def equalRatio2DesiredSize(raw_shape, desired_size= 416):
    ratio = float(desired_size) / max(raw_shape)
    xn, yn = tuple([int(x * ratio) for x in raw_shape])
    xr2t, yr2t = xn / raw_shape[0], yn / raw_shape[1]
    shape_ratio = (xr2t, yr2t)
    new_shape = (xn, yn)

    return new_shape, shape_ratio

def getImageOffset(new_shape, desired_size= 416):
    delta_w = desired_size - new_shape[0]
    delta_h = desired_size - new_shape[1]
    offset_top, offset_bottom = delta_h // 2, delta_h - (delta_h // 2)
    offset_left, offset_right = delta_w // 2, delta_w - (delta_w // 2)
    offset = (offset_top, offset_bottom, offset_left, offset_right)

    return offset

def Images2array(filename=None, image_raw=None, desired_size= 416):
    # https://stackoverflow.com/questions/49466033/resizing-image-and-its-bounding-box
    # filenamelist = glob.glob(".\\input\\" + "*.png")
    # filename = filenamelist[0]

    # Shape Ratio
    if filename is not None:
        image_raw = cv2.imread(filename, 3).astype(np.float)
    image_raw_array = np.array(image_raw, dtype='uint8')
    yr, xr = image_raw.shape[:2]
    raw_shape = (xr, yr)
    new_shape, shape_ratio = equalRatio2DesiredSize(raw_shape, desired_size=desired_size)
    """
    ratio = float(desired_size) / max(raw_shape)
    xn, yn = tuple([int(x * ratio) for x in raw_shape])
    xr2t, yr2t = xn / raw_shape[0], yn / raw_shape[1]
    shape_ratio = (xr2t, yr2t)
    """

    #BGR Image Array
    b, g, r = cv2.split(image_raw)
    rgb_image_raw = cv2.merge([r, g, b])

    # BGR Reshape Image Array
    rgb_image_raw = cv2.resize(rgb_image_raw, new_shape)
    offset = getImageOffset(new_shape, desired_size=desired_size)
    """
    delta_w = desired_size - new_shape[0]
    delta_h = desired_size - new_shape[1]
    offset_top, offset_bottom = delta_h // 2, delta_h - (delta_h // 2)
    offset_left, offset_right = delta_w // 2, delta_w - (delta_w // 2)
    offset = (offset_top, offset_bottom, offset_left, offset_right)
    """

    color = [0, 0, 0]
    resize_image_raw = cv2.copyMakeBorder(rgb_image_raw, offset[0], offset[1], offset[2], offset[3],
                                          cv2.BORDER_CONSTANT, value=color)

    #resize_image_raw = cv2.resize(rgb_image_raw, (416, 416))
    #if xr2t*yr2t > 1:
    #    resize_image_raw = cv2.resize(rgb_image_raw, (416, 416), interpolation =cv2.INTER_CUBIC)
    #elif xr2t*yr2t < 1:
    #    resize_image_raw = cv2.resize(rgb_image_raw, (416, 416), interpolation =cv2.INTER_LANCZOS4), interpolation=cv2.INTER_LANCZOS4
    #else:

    #resize_image_array = np.array(resize_image_raw, dtype='uint8')

    image_array = np.reshape(np.array(resize_image_raw, dtype='uint8'), (1, 416, 416, 3))
    #print(image_array)
    return image_raw_array, image_array, raw_shape, shape_ratio, offset

def IoU(box1, box2):
    w1, h1 = box1
    w2, h2 = box2
    iou = min(w1, w2) * min(h1, h2)
    return iou

def which_anchor(box):
    anchor = [(10, 13), (16, 30), (33, 23),
              (30, 61), (62, 45), (59, 119),
              (116, 90), (156, 198), (373, 326)]
    dist = []

    for i in range(len(anchor)):
        dist.append(IoU(anchor[i], box))
    i = dist.index(max(dist))
    return i

def info2rawlabel(label_info, max_group_num, shape_ratio, shape_offset):
    #classes = 20
    group_num = len(label_info)//5
    label = np.zeros((max_group_num, 6))
    #print(label.shape)
    for idx in range(0, group_num):
        xmin, ymin, xmax, ymax, obj_num = label_info[5*idx:5*(idx+1)]
        xmin, ymin, xmax, ymax = int(xmin), int(ymin), int(xmax), int(ymax)

        xmin, xmax = (xmin + shape_offset[2]) * shape_ratio[0], (xmax + shape_offset[2]) * shape_ratio[0]
        ymin, ymax = (ymin + shape_offset[0]) * shape_ratio[1], (ymax + shape_offset[0]) * shape_ratio[1]
        x0, y0 = (xmin+xmax)*0.5, (ymin+ymax)*0.5
        w0, h0 = xmax-xmin, ymax-ymin
        cls = int(obj_num)
        label[idx] = [1, x0, y0, w0, h0, cls]
    return label

def getLabelArray(box_info, cls, anchor_idx, out_base_whd, Y, desired_size= (416, 416)):
    wdx = int(np.floor((box_info[0] / desired_size[0]) * out_base_whd[0]))
    hdx = int(np.floor((box_info[1] / desired_size[1]) * out_base_whd[1]))
    #print(Y.shape, wdx, hdx)
    Y[0, hdx, wdx, 0 + anchor_idx * (out_base_whd[2] // 3)] = box_info[0]
    Y[0, hdx, wdx, 1 + anchor_idx * (out_base_whd[2] // 3)] = box_info[1]
    Y[0, hdx, wdx, 2 + anchor_idx * (out_base_whd[2] // 3)] = box_info[2]
    Y[0, hdx, wdx, 3 + anchor_idx * (out_base_whd[2] // 3)] = box_info[3]
    Y[0, hdx, wdx, 4 + anchor_idx * (out_base_whd[2] // 3)] = 1
    Y[0, hdx, wdx, 5 + cls + anchor_idx * (out_base_whd[2] // 3)] = 1

    return Y

def rawlabel2Array(labels, n_classes = 20, desired_size= (416, 416)):
    out_height = 416 // 32
    out_width = 416 // 32
    out_depth = 3 * (5 + n_classes)
    Y1 = np.zeros((1, out_height, out_width, out_depth))
    Y2 = np.zeros((1, 2 * out_height, 2 * out_width, out_depth))
    Y3 = np.zeros((1, 4 * out_height, 4 * out_width, out_depth))
    #print(labels)
    for obj in labels:
        confidence, x0, y0, w0, h0, cls = obj
        if int(confidence) is 0:
            break
        x0, y0, w0, h0, cls = int(x0), int(y0), int(w0), int(h0), int(cls)
        #print(confidence, x0, y0, w0, h0, cls)
        box = (w0, h0)
        idx = which_anchor(box)
        #print(box, idx)
        if idx >= 6:  # anchor 6-9
            #print( idx, x0, y0, w0, h0)
            Y1 = getLabelArray((x0, y0, w0, h0), cls, idx-6, (out_width, out_height, out_depth), Y1,
                               desired_size=desired_size)
        elif (idx >= 3) and (idx < 6):  # anchor 3-6
            Y2 = getLabelArray((x0, y0, w0, h0), cls, idx-3, (2*out_width, 2*out_height, out_depth), Y2,
                               desired_size=desired_size)
        else: # anchor 1-3
            #print(idx, x0, y0, w0, h0)
            Y3 = getLabelArray((x0, y0, w0, h0), cls, idx, (4*out_width, 4*out_height, out_depth), Y3,
                               desired_size=desired_size)
    return Y1, Y2, Y3

def get_voc_info_list(info_filename):
    file = open(info_filename, 'r')
    done = bool(0)
    info_list = []
    while not done:
        this_info = file.readline()
        this_info_array = this_info.split()
        if len(this_info_array) < 1:
            done = bool(1)
            file.close()
        else:
            info_list.append(this_info_array)

    return info_list

def check_box_images_label(rgb_image, label, imshow_flag = True):
    #print(label)
    r, g, b = cv2.split(rgb_image)
    bgr_image = cv2.merge([b, g, r])
    for idx in range(0, len(label)):
        # changed color and width to make it visible
        conf, x0, y0, w0, h0, cls = label[idx]
        xmin = int(x0-w0*0.5)
        ymin = int(y0-h0*0.5)
        xmax = int(x0+w0*0.5)
        ymax = int(y0+h0*0.5)
        #print(xmin, ymin, xmax, ymax)
        cv2.rectangle(bgr_image, (xmin, ymin), (xmax, ymax), (255, 0, 0), 1)
    if imshow_flag:
        cv2.imshow("img", bgr_image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

def find_image_max_obj(fileinfo_list, read_num):
    max_group_num = 0
    for idx in range(read_num):
        fileinfo = fileinfo_list[idx]
        this_group_num = len(fileinfo[1:])//5
        if this_group_num > max_group_num:
            max_group_num = this_group_num
    return max_group_num

def voc_to_tfrecord(tfrecord_file_name, fileinfo_list, read_num, image_check_flag):
    images, labels = [], []
    #max_group_num = find_image_max_obj(fileinfo_list, read_num)
    max_group_num = 45
    each_run_num = 5

    for idx in range(read_num):
        fileinfo = fileinfo_list[idx]
        image_raw_array, image_array, raw_shape, shape_ratio, offset = Images2array(fileinfo[0])
        labels.append(info2rawlabel(fileinfo[1:], max_group_num, shape_ratio, offset))
        #print(fileinfo[0])
        for jdx in range(each_run_num):
            image_array, raw_shape, shape_ratio, shape_offset, image_limit_shape, rot_angle, cropping_size, flip_para = \
                croppingImages2array(fileinfo[0])

            labelslist = croppingInfo2rawlabel(fileinfo[1:], raw_shape, max_group_num, shape_ratio, shape_offset,
                                               image_limit_shape,
                                               rot_angle, cropping_size, flip_para)
            labels.append(labelslist)
            images.append(image_array)
            check_box_images_label(np.resize(image_array, (416, 416, 3)),
                                   labels[idx*(each_run_num+1)+jdx+1],
                                   imshow_flag = image_check_flag)

        #Y1[idx] = label1
        #Y2[idx] = label2
        #print(count, len(images), len(Y1), len(Y2))
        #check_box_images_label(np.resize(image_array, (416, 416, 3)), labels[idx])

    #print(len(labels), len(labels[0]), len(labels[1]))
    print("Data to Array: Completed!")
    tfr.convert_multiple_images(tfrecord_file_name,
                                read_num,
                                __voc_image_2_tfrecord_format__,
                                images,
                                labels)

def voc2TFRecord(raw_file_folder, destdir, tfrecord_file_name, image_check_flag = True, save_img_num = -1):
    #destdir = os.path.join('.', destdir)
    CU.createOutputFolder(destdir)
    classes_name, classes_num = voc_all_class()
    filepath = os.path.join(destdir, "*.tfrecords")

    if (len(glob.glob(filepath)) < 2):
        info_filename = set_voc_info_file(classes_num, data_dir=raw_file_folder)
        fileinfo_list = get_voc_info_list(info_filename)
        read_num, max_num = save_img_num, len(fileinfo_list)
        if (save_img_num > max_num) or (save_img_num == -1):
            read_num = max_num

        voc_to_tfrecord(destdir+tfrecord_file_name, fileinfo_list, read_num, image_check_flag)
    print("All Database is Already!")

def minivoc2TFRecord(raw_file_folder, destdir, tfrecord_file_name, image_check_flag = True, save_img_num = -1):
    CU.createOutputFolder(destdir)
    tfrecord_files_path = os.path.join(destdir, "*.tfrecords")
    tfrecord_file_path = os.path.join(destdir, tfrecord_file_name)
    classes_name, classes_num = mini_voc_class()

    if (len(glob.glob(tfrecord_files_path)) < 2):
        info_filename = set_voc_info_file(classes_num, data_dir=raw_file_folder)
        fileinfo_list = get_voc_info_list(info_filename)
        read_num, max_num = save_img_num, len(fileinfo_list)
        if (save_img_num > max_num) or (save_img_num == -1):
            read_num = max_num

        voc_to_tfrecord(tfrecord_file_path, fileinfo_list, read_num, image_check_flag)
    print("All Database is Already!")

def TFRecordOneShot2BacthDataset(tfrecord_filename, total_num, shuffle_size, repeat_num, batch_size, n_classes):
    # Extracting the Images
    image_info = tfr.one_shot_extract_multiple_images(__tfrecord_2_image_format__,
                                                      tfrecord_filename,
                                                      shuffle_size,
                                                      repeat_num,
                                                      batch_size)
    #print(image_info)

    # Initialization
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    sess.run(tf.local_variables_initializer())

    # Coordinator Initialization
    coord = tf.train.Coordinator()

    # Initialization
    Y1_dataset = np.random.random((total_num, 13, 13, 3 * (5 + n_classes)))
    Y2_dataset = np.random.random((total_num, 26, 26, 3 * (5 + n_classes)))
    Y3_dataset = np.random.random((total_num, 52, 52, 3 * (5 + n_classes)))
    image_dataset = np.random.random((total_num, 416, 416, 3))

    #total_num = 5
    for idx in range(0, total_num, batch_size):
        if not coord.should_stop():
            image, raw_labels, img_shape, label_shape = sess.run(image_info)
            #print(image.shape, raw_labels.shape)
            for jdx in range(len(raw_labels)):
                #print(raw_labels[jdx])
                this_Y1, this_Y2, this_Y3 =rawlabel2Array(raw_labels[jdx], n_classes = n_classes)
                #print(this_Y1)
                #print(this_Y2)
                #print(this_Y3)
                Y1_dataset[idx+jdx] = this_Y1
                Y2_dataset[idx+jdx] = this_Y2
                Y3_dataset[idx+jdx] = this_Y3
                image_dataset[(idx//batch_size)*batch_size+jdx] = image[jdx]
            #print(image_dataset.shape, Y1_dataset.shape, Y2_dataset.shape)
    sess.close()
    return image_dataset, Y1_dataset, Y2_dataset, Y3_dataset

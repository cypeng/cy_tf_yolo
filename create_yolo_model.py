# -*- coding: utf-8 -*-
"""
@author: CY Peng

DataVer  - Author - Note
2019/06/07    CY    Training/Testing/Validation OK
2019/11/26    CY    Lib Path Update
"""
import CommonUtility as CU
import tensorflow as tf
import preprocessingLib as PrePro
import Yolo_v3 as yolo
import os
import glob
import postprocessingLib as PostPro
import numpy as np
import cv2
from seaborn import color_palette
#import logging
os.environ['CUDA_VISIBLE_DEVICES'] = '0'
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

def training_stage(sess, algObj, dataset_x, dataset_y1, dataset_y2, dataset_y3, batch_size):
    for offset in range(0, dataset_x.shape[0], batch_size):
        end = offset + batch_size
        batch_x, batch_y1, batch_y2, batch_y3 = \
            dataset_x[offset:end], dataset_y1[offset:end], dataset_y2[offset:end], dataset_y3[offset:end]

        """
        [test1, test2, test3, test4, test5, test6, test7, test8, test9, test10] = \
            sess.run([algObj.test1, algObj.test2, algObj.test3, algObj.test4,
                      algObj.test5, algObj.test6, algObj.test7, algObj.test8,
                      algObj.test9, algObj.test10], feed_dict={algObj.x: batch_x,
                                                               algObj.y1: batch_y1,
                                                               algObj.y2: batch_y2,
                                                               algObj.y3: batch_y3})

        print(test1)
        print(test2)
        print(test3)
        print(test4)
        print(test5)
        print(test6)
        print(test7)
        print(test8)
        print(test9)
        print(test10)
        """


        sess.run([algObj.training_operation], feed_dict={algObj.x: batch_x,
                                                         algObj.y1: batch_y1,
                                                         algObj.y2: batch_y2,
                                                         algObj.y3: batch_y3})

def evaluation_stage(sess,
                     feed_x,
                     feed_y1,
                     feed_y2,
                     feed_y3,
                     loss_operation,
                     dataset_x,
                     dataset_y1,
                     dataset_y2,
                     dataset_y3,
                     this_epoch,
                     batch_size,
                     writer = None):
    log_file = open("training_log.txt", 'a')
    run_loss, count,  run_fscore, fscore, loss_monitor,conv_cond_monitor = 0, 0, 0, 0, [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]
    for offset in range(0, dataset_x.shape[0], batch_size):
        end = offset + batch_size
        batch_x, batch_y1, batch_y2, batch_y3  = \
            dataset_x[offset:end], dataset_y1[offset:end], dataset_y2[offset:end], dataset_y3[offset:end]
        estimation_result  = sess.run(loss_operation,
                                  feed_dict={feed_x: batch_x,
                                             feed_y1: batch_y1,
                                             feed_y2: batch_y2,
                                             feed_y3: batch_y3})
        xy_loss, wh_loss, conf_pos_loss, conf_neg_loss,class_loss, \
        precision, recall, iou, pos_tar_num, neg_tar_num = estimation_result
        fscore = 2*precision*recall/(precision+recall)
        loss = [xy_loss, wh_loss, conf_pos_loss, conf_neg_loss, class_loss]
        conv_cond = [precision, recall, iou, pos_tar_num, neg_tar_num]
        total_loss = xy_loss+wh_loss+conf_pos_loss+conf_neg_loss+class_loss

        run_loss += total_loss
        run_fscore += fscore
        for idx in range(5):
            conv_cond_monitor[idx] += conv_cond[idx]
        for idx in range(5):
            loss_monitor[idx] += loss[idx]

        #print(loss[0], loss[1], loss[2], loss[3])
        if writer is not None:
            summary = tf.Summary(value=[
                tf.Summary.Value(tag="xy_loss_val", simple_value=loss[0]),
                tf.Summary.Value(tag="wh_loss_val", simple_value=loss[1]),
                tf.Summary.Value(tag="conf_pos_loss_val", simple_value=loss[2]),
                tf.Summary.Value(tag="conf_neg_loss_val", simple_value=loss[3]),
                tf.Summary.Value(tag="class_loss_val", simple_value=loss[4]),
                tf.Summary.Value(tag="precision", simple_value=conv_cond[0]),
                tf.Summary.Value(tag="recall", simple_value=conv_cond[1]),
                tf.Summary.Value(tag="iou", simple_value=conv_cond[2]),
                tf.Summary.Value(tag="f_score", simple_value=fscore),
                tf.Summary.Value(tag="pos_num", simple_value=conv_cond[3]),
                tf.Summary.Value(tag="neg_num", simple_value=conv_cond[4]),
                tf.Summary.Value(tag="total_loss_val", simple_value=total_loss),
            ])
            writer.add_summary(summary, this_epoch)

        count += 1
        print("EPOCH %d BatchSizeOffset %d, xy Loss: %f, wh Loss: %f, "
              "conf pos Loss: %f, conf neg Loss: %f, class Loss: %f, total Loss: %f" %
              (this_epoch, offset, loss[0], loss[1], loss[2], loss[3], loss[4], total_loss))
        print("EPOCH %d BatchSizeOffset %d, precision: %f, recall: %f, iou: %f, f-score: %f for pos/neg target: %f/%f" %
              (this_epoch, offset, conv_cond[0], conv_cond[1], conv_cond[2], fscore, pos_tar_num, neg_tar_num))
        log_file.write("EPOCH {} BatchSizeOffset {}, xy Loss: {}, wh Loss: {}, conf pos Loss: {}, conf neg Loss: {}, class Loss: {}, total Loss: {}\n".format(this_epoch, offset, loss[0], loss[1], loss[2], loss[3], loss[4], total_loss))
        log_file.write("EPOCH {} BatchSizeOffset {}, precision: {}, recall: {}, iou: {}, f-score: {} for pos/neg target: {}/{}\n".format(this_epoch, offset, conv_cond[0], conv_cond[1], conv_cond[2], fscore, pos_tar_num, neg_tar_num))
    run_loss /= count
    run_fscore /= count
    for idx in range(5):
        conv_cond_monitor[idx] /= count
    for idx in range(5):
        loss_monitor[idx] /= count

    print("EPOCH %d: xy Loss: %f, wh Loss: %f, conf pos Loss: %f, conf neg Loss: %f, class Loss: %f, total Loss: %f" %
        (this_epoch, loss_monitor[0], loss_monitor[1], loss_monitor[2], loss_monitor[3], loss_monitor[4], run_loss))
    print("EPOCH %d: precision: %f, recall: %f, iou: %f, f-score: %f for pos/neg target: %f/%f" %
          (this_epoch, conv_cond_monitor[0], conv_cond_monitor[1], conv_cond_monitor[2], run_fscore, conv_cond_monitor[3], conv_cond_monitor[4]))
    log_file.write("EPOCH {}: xy Loss: {}, wh Loss: {}, conf pos Loss: {}, conf neg Loss: {}, class Loss: {}, total Loss: {}\n".format(this_epoch, loss_monitor[0], loss_monitor[1], loss_monitor[2], loss_monitor[3], loss_monitor[4], run_loss))
    log_file.write("EPOCH {}: precision: {}, recall: {}, iou: {}, f-score: {} for pos/neg target: {}/{}\n".format(
                   this_epoch, conv_cond_monitor[0], conv_cond_monitor[1], conv_cond_monitor[2], run_fscore, conv_cond_monitor[3], conv_cond_monitor[4]))
    log_file.close()

def restore_part_variables(meta_dir):
    reader = tf.train.NewCheckpointReader(meta_dir + "model.ckpt")
    modvarlist, glovarlist, revarlist, inivarlist = [], [], [], []
    var_to_shape_map = reader.get_variable_to_shape_map()
    glovar = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
    for key in sorted(var_to_shape_map):
        modvarlist.append(key)
    for idx in range(0, len(glovar)):
        glovarlist.append(glovar[idx].name.split(":0")[0])
    for idx in range(0, len(glovar)):
        var_name = glovarlist[idx]
        if var_name in modvarlist:
            revarlist.append(glovar[idx])
        else:
            inivarlist.append(glovar[idx])
    return revarlist, inivarlist

def TFRecord2TraningYoloModel(training_db_filename,
                               test_db_filename,
                               epoch_num,
                               shuffle_size,
                               repeat_num,
                               batch_size,
                               meta_dir =  ".\\output\\model_0\\training\\",
                               output_dir = ".\\output\\",
                               total_training_sample_num = 2000,
                               total_test_sample_num = 1000):
    # Output Folder Check
    model_output_folder = CU.updateOutputFolder(output_dir, outputfolder="model_")

    output_base_folder = model_output_folder + "\\translearning\\"
    ckpt_filename = output_base_folder + "model.ckpt"
    log_dir = output_base_folder + "train_logs\\"

    # Model Initialization
    # ["yolo_v3/new_conv_layer58", "yolo_v3/new_conv_layer66", "yolo_v3/new_conv_layer74"]
    train_var_scope_list = ["yolo_v3/new_conv_layer58", "yolo_v3/new_conv_layer66", "yolo_v3/new_conv_layer74"]
    YoLoAlg = yolo.Yolo_v3(learning_rate = 0.0001, batch_size=batch_size, n_classes = 2, transfer_learning_flag = True)

    #print(set(tf.all_variables()))
    #print(tf.trainable_variables(scope=None))
    YoLoAlg.set_tran_variable_scope(var_list=train_var_scope_list)

    # Only log errors (to prevent unnecessary cluttering of the console)
    tf.logging.set_verbosity(tf.logging.ERROR)

    # Variables Initialization
    var_ini_operation = [tf.global_variables_initializer(), tf.local_variables_initializer()]

    # Training Obejct Saver Establishment
    restore_var_list, ini_var_list = restore_part_variables(meta_dir)
    #print(restore_var_list, ini_var_list)
    old_saver = tf.train.Saver(restore_var_list)
    saver = tf.train.Saver()
    #print(ini_var_list)
    part_var_ini_operation = tf.initialize_variables(ini_var_list)
    #print(part_var_ini_operation)
    # tf.InteractiveSession()
    #print(ini_var_list)

    with tf.Session() as train_sess:
        train_sess.run(var_ini_operation)
        old_saver.restore(train_sess, tf.train.latest_checkpoint(meta_dir))
        train_sess.run(part_var_ini_operation)

        train_writer = tf.summary.FileWriter(log_dir, train_sess.graph)
        tf.train.write_graph(train_sess.graph.as_graph_def(), model_output_folder, 'model.pbtxt', as_text=True)
        tf.train.write_graph(train_sess.graph.as_graph_def(), model_output_folder, 'model.pb', as_text=False)
        for epoch_now in range(0, epoch_num):
            print("EPOCH {} Start: Set the Dataset".format(epoch_now + 1))
            # Dataset Preparation
            image_training_dataset, label_train_dataset1, label_train_dataset2, label_train_dataset3 = \
                PrePro.TFRecordOneShot2BacthDataset(training_db_filename,
                                                    total_training_sample_num,
                                                    shuffle_size,
                                                    repeat_num,
                                                    batch_size,
                                                    YoLoAlg.n_classes)
            image_test_dataset, label_test_dataset1, label_test_dataset2, label_test_dataset3 = \
                image_training_dataset, label_train_dataset1, label_train_dataset2, label_train_dataset3
            #print(np.where(label_train_dataset1==1))
            #print(np.where(label_train_dataset2 == 1))
            #print(np.where(label_train_dataset3 == 1))
            """
            image_test_dataset, label_test_dataset1, label_test_dataset2, label_test_dataset3 = \
                PrePro.TFRecordOneShot2BacthDataset(test_db_filename,
                                                    total_test_sample_num,
                                                    shuffle_size,
                                                    repeat_num,
                                                    batch_size)
            """


            """
            prediction_output = train_sess.run(YoLoAlg.y, feed_dict={YoLoAlg.x: image_test_dataset,
                                                                     YoLoAlg.y1: label_test_dataset1,
                                                                     YoLoAlg.y2: label_test_dataset2,
                                                                     YoLoAlg.y3: label_test_dataset3})
            print(prediction_output)
            detection_result = train_sess.run(
                PostPro.predict_boxes(prediction_output, 1, iou_threshold=.3, confidence_threshold=.3),
                feed_dict={YoLoAlg.x: image_test_dataset})
            print(detection_result)
            boxes = detection_result[0]
            colors = ((np.array(color_palette("hls", 1)) * 255)).astype(np.uint8)
            this_boxes=boxes[0]
            print(this_boxes[0])
            for this_box in this_boxes:
                print(this_box)
                xmin, ymin, xmax, ymax, confidence = this_box[0], this_box[1], this_box[2], this_box[3], this_box[4]
                this_color = (int(colors[0, 0]), int(colors[0, 1]), int(colors[0, 2]))
                cv2.rectangle(image_test_dataset[0], (xmin, ymin), (xmax, ymax), this_color, 3)
            r, g, b = cv2.split(image_test_dataset[0])
            bgr_image_raw = cv2.merge([b, g, r])
            bgr_image_raw = np.array(bgr_image_raw, dtype='uint8')
            cv2.imshow("img", bgr_image_raw)
            cv2.waitKey(0)
            cv2.destroyAllWindows()
            """
            #PostPro.pil_image_draw_boxes(filename, detection_result[0], ["persion"], shape_ratio, offset)

            for repeat_now in range(repeat_num):
                print("EPOCH {}-repeat_num-{} Traning Start...".format(epoch_now + 1, repeat_now+1))
                # Training
                training_stage(train_sess,
                               YoLoAlg,
                               image_training_dataset,
                               label_train_dataset1,
                               label_train_dataset2,
                               label_train_dataset3,
                               batch_size)

            # Testing
            print("EPOCH {} Testing Start".format(epoch_now + 1))
            evaluation_stage(train_sess,
                             YoLoAlg.x,
                             YoLoAlg.y1,
                             YoLoAlg.y2,
                             YoLoAlg.y3,
                             YoLoAlg.loss_operation_monitor,
                             image_test_dataset,
                             label_test_dataset1,
                             label_test_dataset2,
                             label_test_dataset3,
                             epoch_now + 1,
                             batch_size,
                             writer=train_writer)
            saver.save(train_sess, ckpt_filename, global_step=epoch_now)
            saver.save(train_sess, ckpt_filename)

        train_sess.close()
        train_writer.close()

        print("Model saved")
        print('All Threads Are Stopped!')

def ModelValidation(validation_db_filename,
                    model_base_folder,
                    shuffle_size,
                    repeat_num,
                    batch_size,
                    n_classes,
                    total_validation_sample_num=1000):
    # Validation - Dataset Preparation
    model_folder = model_base_folder + "\\translearning\\"
    if not os.path.isdir(model_folder):
        model_folder = model_base_folder + "\\translearning\\"
    val_meta_file = model_folder + "\\model.ckpt.meta"

    # Model Reading
    val_saver = tf.train.import_meta_graph(val_meta_file, clear_devices=True)

    with tf.Session() as val_sess:
        val_saver.restore(val_sess, tf.train.latest_checkpoint(model_folder))
        #print(tf.all_variables())

        graph = tf.get_default_graph()
        x = graph.get_tensor_by_name("input/x:0")
        y1 = graph.get_tensor_by_name("output/label_1:0")
        y2 = graph.get_tensor_by_name("output/label_2:0")
        y3 = graph.get_tensor_by_name("output/label_3:0")
        loss_op = tf.get_collection("loss_operation_monitor")[0]
        #print(x,y1, y2, y3)
        #print(loss_op)
        #print(x,y, loss_op, accuracy_op)

        # Validation
        print("Validation Start...")
        image_training_dataset, label_val_dataset1, label_val_dataset2, label_val_dataset3 = \
            PrePro.TFRecordOneShot2BacthDataset(validation_db_filename,
                                                total_validation_sample_num,
                                                shuffle_size,
                                                repeat_num,
                                                batch_size,
                                                n_classes)

        evaluation_stage(val_sess,
                         x,
                         y1,
                         y2,
                         y3,
                         loss_op,
                         image_training_dataset,
                         label_val_dataset1,
                         label_val_dataset2,
                         label_val_dataset3,
                         0,
                         batch_size,
                         writer = None)

        builder = tf.saved_model.builder.SavedModelBuilder(model_base_folder+"\\validation\\")
        builder.add_meta_graph_and_variables(val_sess,
                                             [tf.saved_model.tag_constants.TRAINING],
                                             signature_def_map=None,
                                             assets_collection=None)
        builder.save()
        val_sess.close()
        print("Validation End...")

def model2prediction(voc_images_folder, class_names, model_base_folder=os.path.join(".","output"), coco_labels_file=os.path.join(".","coco.names")):
    # Prediction - Model Dataset Folder
    model_folder = os.path.join(model_base_folder, "validation") #model_base_folder + "\\validation\\"
    print(model_folder)
    filename_list = glob.glob(voc_images_folder + "*.jpg")
    check_flag = bool(1)
    #class_names = PrePro.classes_name

    with tf.Session() as pred_sess:
        tf.saved_model.loader.load(pred_sess, [tf.saved_model.tag_constants.TRAINING], model_folder)
        graph = tf.get_default_graph()
        x = graph.get_tensor_by_name("input/x:0")
        prediction_op = graph.get_tensor_by_name("yolo_v3/concat:0")

        for filename in filename_list:
            image_raw_array, image_array, raw_shape, shape_ratio, offset = PrePro.Images2array(filename=filename)
            # print(image_array.shape)
            prediction_output = pred_sess.run(prediction_op, feed_dict={x: image_array})
            detection_result = pred_sess.run(PostPro.predict_boxes(prediction_output, 1,iou_threshold = .5, confidence_threshold = .65),
                                             feed_dict={x: image_array})
            print(detection_result)
            PostPro.pil_image_draw_boxes(filename, detection_result[0], class_names, shape_ratio, offset, check_flag)
            if check_flag:
                check_flag = bool(0)
    print("Prediction End...")

"""
def webcam_model2prediction(class_names, model_base_folder=".\\output", coco_labels_file=".\\coco.names"):
    # Prediction - Model Dataset Folder
    model_folder = model_base_folder + "\\validation\\"
    cap = cv2.VideoCapture(0)
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    #config.gpu_options.per_process_gpu_memory_fraction = 0.4  # 占用GPU40%的显存

    count = 0
    pred_sess = tf.Session(config=config)
    tf.saved_model.loader.load(pred_sess, [tf.saved_model.tag_constants.TRAINING], model_folder)
    graph = tf.get_default_graph()
    x = graph.get_tensor_by_name("input/x:0")
    prediction_op = graph.get_tensor_by_name("yolo_v3/concat:0")
    while (True):
        ret, frame = cap.read()
        if count % 3 == 0:
            cv2.imshow('Webcame', frame)
        # print(image_array.shape)
        if count % 30 == 0:
            image_raw_array, image_array, raw_shape, shape_ratio, offset = PrePro.Images2array(image_raw=frame)
            prediction_output = pred_sess.run(prediction_op, feed_dict={x: image_array})
            detection_result = pred_sess.run(
                PostPro.predict_boxes(prediction_output, 1, iou_threshold=.8, confidence_threshold=.995),
                feed_dict={x: image_array})
        # print(detection_result)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        count += 1
        count = count % 60

    cap.release()
    cv2.destroyAllWindows()
    print("End...")
"""

# -*- coding: utf-8 -*-
"""
@author: CY Peng

DataVer  - Author - Note
2019/06/05    CY    Pretraining/Testing/Validation/Prediction OK
"""
import tensorflow as tf
from PIL import Image, ImageDraw, ImageFont
from seaborn import color_palette
import numpy as np
import cv2
import os
import CommonUtility as CU

def predict_boxes(ypred, batch_size,
                  iou_threshold = .5, confidence_threshold = .5,
                  n_classes = 80, max_output_size = 20):
    boxes = build_boxes(ypred)
    boxes_dicts = non_max_suppression(boxes, batch_size, n_classes,
                                      iou_threshold = iou_threshold, confidence_threshold = confidence_threshold,
                                      max_output_size = max_output_size)
    return boxes_dicts

def build_boxes(ypred):
    center_x, center_y, width, height, confidence, classes = \
        tf.split(ypred, [1, 1, 1, 1, 1, -1], axis=-1)

    top_left_x = center_x - width / 2
    top_left_y = center_y - height / 2
    bottom_right_x = center_x + width / 2
    bottom_right_y = center_y + height / 2

    boxes = tf.concat([top_left_x, top_left_y,
                       bottom_right_x, bottom_right_y,
                       confidence, classes], axis=-1)
    return boxes

def non_max_suppression(batch_boxes, batch_size, n_classes, iou_threshold = .5, confidence_threshold = .5,
                        max_output_size = 20):
    # batch = tf.unstack(boxes)

    boxes_dicts = []
    for this_idx in range(batch_size):
        boxes = batch_boxes[this_idx]
        boxes = tf.boolean_mask(boxes, boxes[:, 4] > confidence_threshold)
        classes = tf.argmax(boxes[:, 5:], axis=-1)
        classes = tf.expand_dims(tf.to_float(classes), axis=-1)
        boxes = tf.concat([boxes[:, :5], classes], axis=-1)

        boxes_dict = dict()
        # boxes_list = []
        for cls in range(n_classes):
            mask = tf.equal(boxes[:, 5], cls)
            mask_shape = mask.get_shape()
            if mask_shape.ndims != 0:
                class_boxes = tf.boolean_mask(boxes, mask)
                boxes_coords, boxes_conf_scores, _ = tf.split(class_boxes,
                                                              [4, 1, -1],
                                                              axis=-1)
                boxes_conf_scores = tf.reshape(boxes_conf_scores, [-1])
                indices = tf.image.non_max_suppression(boxes_coords,
                                                       boxes_conf_scores,
                                                       max_output_size,
                                                       iou_threshold)
                class_boxes = tf.gather(class_boxes, indices)
                # print(class_boxes[:, :5])
                # boxes_list.append(class_boxes[:, :5])
                # boxes_array[this_idx, cls, :] = class_boxes[:, :5]
                boxes_dict[cls] = class_boxes[:, :5]
            # else:
            #    boxes_list.append(None)
        boxes_dicts.append(boxes_dict)

    return boxes_dicts

def cv_image_draw_boxes(image_filename, boxes_dict, class_names, shape_ratio, shape_offset, check_flag):
    image_raw = cv2.imread(image_filename, 3).astype(np.float)
    image_raw_array = np.array(image_raw, dtype='uint8')
    colors = ((np.array(color_palette("hls", len(class_names))) * 255)).astype(np.uint8)
    fontScale = (image_raw.size[0] + image_raw.size[1])//100
    for cls in range(len(class_names)):
        boxes = boxes_dict[cls]

        if np.size(boxes) != 0:
            for this_box in boxes:
                xmin, ymin, xmax, ymax, confidence = this_box[0], this_box[1], this_box[2], this_box[3], this_box[4]
                #print(class_names[cls], confidence)
                xmin, xmax = int((xmin-shape_offset[2]) / shape_ratio[0]), int((xmax-shape_offset[2]) / shape_ratio[0])
                ymin, ymax = int((ymin-shape_offset[0]) / shape_ratio[1]), int((ymax-shape_offset[0]) / shape_ratio[1])
                text = '{} {:.1f}%'.format(class_names[cls], confidence * 100)
                this_color = (int(colors[cls, 0]), int(colors[cls, 1]), int(colors[cls, 2]))
                cv2.putText(image_raw_array, text, (xmin, ymax), cv2.FONT_HERSHEY_SIMPLEX, fontScale, (0, 0, 0), 3)
                cv2.rectangle(image_raw_array, (xmin, ymin), (xmax, ymax), this_color, 3)

    #cv2.imshow("img", image_array)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    image_filename_list = image_filename.split("\\")
    image_filename_list.insert(-1, "results")
    save_image_filename = os.path.join(*image_filename_list)
    if check_flag:
        CU.createOutputFolder(os.path.join(*image_filename_list[0:-1]))
    cv2.imwrite(save_image_filename.replace(".jpg", "_pred.jpg"), image_raw_array)

def pil_image_draw_boxes(image_filename, boxes_dict, class_names, shape_ratio, shape_offset, check_flag):
    colors = ((np.array(color_palette("hls", len(class_names))) * 255)).astype(np.uint8)
    image = Image.open(image_filename)
    #print(image_filename)
    draw_image = ImageDraw.Draw(image)
    size = (image.size[0] + image.size[1]) // 100
    fontScale = ImageFont.truetype(font='.\\offical_elements\\futur.ttf',
                                   size=size)
    for cls in range(len(class_names)):
        boxes = boxes_dict[cls]

        if np.size(boxes) != 0:
            for this_box in boxes:
                xmin, ymin, xmax, ymax, confidence = this_box[0], this_box[1], this_box[2], this_box[3], this_box[4]
                print(class_names[cls], confidence)
                xmin, xmax = int((xmin-shape_offset[2]) / shape_ratio[0]), int((xmax-shape_offset[2]) / shape_ratio[0])
                ymin, ymax = int((ymin-shape_offset[0]) / shape_ratio[1]), int((ymax-shape_offset[0]) / shape_ratio[1])
                text = '{} {:.1f}%'.format(class_names[cls], confidence * 100)
                text_size = draw_image.textsize(text, font=fontScale)
                this_color = (int(colors[cls, 0]), int(colors[cls, 1]), int(colors[cls, 2]))
                draw_image.rectangle([(xmin, ymin - text_size[1]),
                                      (xmax+ text_size[0], ymax)],
                                     outline=tuple(this_color),
                                     width= int(size*0.5))
                draw_image.text((xmin, ymin- text_size[1]), text, fill='black', font=fontScale)
    rgb_image = image.convert('RGB')
    image_filename_list = image_filename.split("\\")
    image_filename_list.insert(-1, "results")
    save_image_filename = os.path.join(*image_filename_list)
    if check_flag:
        CU.createOutputFolder(os.path.join(*image_filename_list[0:-1]))
    rgb_image.save(save_image_filename.replace(".jpg", "_pred.jpg"))
